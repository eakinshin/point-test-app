//
//  MainViewModel.swift
//  PointsTestApp
//
//  Created by EAkinshin on 12.07.2021.
//

import Foundation

class MainViewModel: ObservableObject {
    
    @Published var pointsCountString: String = ""
    
    @Published var pointsChartData: PointsChartData = PointsChartData(points: [])
    
    @Published var fetchDataError: String? = nil
    
    @Published var goToPointsTabBar: Bool = false
    
    @Published var isLoading: Bool = false
    
    func isPointsCountValid() -> Bool {
        return
            (self.pointsCountString.range(of: "^[1-9][0-9]*$", options: .regularExpression) != nil
            || pointsCountString.isEmpty)
            && pointCountsStringToInt() != 1
    }
    
    func isGoButtonDisabled() -> Bool {
        return !isPointsCountValid() || self.pointCountsStringToInt() < 1
    }
    
    private func pointCountsStringToInt() -> Int {
        return Int(pointsCountString) ?? 0
    }
    
    func errorOnValidatePointsCount() -> String {
        guard !self.isPointsCountValid() else {
            return ""
        }
        return "Введите целое число больше 1."
    }
    
    func fetchPoints() {
        DispatchQueue.main.async {
            self.isLoading = true
        }
        DispatchQueue.global(qos: .background).async {
            MobileTestControllerAPI.points(count: self.pointCountsStringToInt()) { data, error in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    
                    self.isLoading = false
                    if let error = error {
                        self.onErrorMessage(message: "main_view_error_prefix".localized().appending(error.localizedDescription))
                        return
                    }
                    guard let pointsResponse = data else {
                        return
                    }
                    self.pointsChartData = PointsChartData(points: pointsResponse.points)
                    self.goToPointsTabBar = true
                }
            }
        }
    }
    
    func onErrorMessage(message: String) {
                
        let delayToHideMessage = 3.0 // in sec
        
        DispatchQueue.main.async {
            self.fetchDataError = "\(message)"
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + delayToHideMessage) {
            self.fetchDataError = nil
        }
    }
}
