//
//  String+Extension.swift
//  PointsTestApp
//
//  Created by EAkinshin on 14.07.2021.
//

import Foundation

extension String {

    public func localized(_ bundle: Bundle? = nil) -> String {
        return NSLocalizedString(self, bundle: bundle ?? Bundle.main, comment: "")
    }
}
