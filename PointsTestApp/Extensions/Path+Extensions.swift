//
//  Path + Extensions.swift
//  PointsTestApp
//
//  Created by EAkinshin on 10.07.2021.
//

import SwiftUI

extension Path {
    
    mutating func drawAxis(screenSize: CGSize, marginPercents: Int) {
        self.move(to: CGPoint(x: screenSize.width / CGFloat(marginPercents * 2), y: screenSize.height / 2))
        self.addLine(to: CGPoint(x: screenSize.width - screenSize.width / CGFloat(marginPercents * 2), y: screenSize.height / 2))
        
        self.move(to: CGPoint(x: screenSize.width / 2, y: screenSize.height / CGFloat(marginPercents * 2)))
        self.addLine(to: CGPoint(x: screenSize.width / 2, y: screenSize.height - screenSize.height / CGFloat(marginPercents * 2)))
    }
    
    mutating func drawNet(
        screenSize: CGSize,
        deltaXValues: CGFloat,
        deltaYValues: CGFloat,
        marginPercents: Int,
        step: Int
    ) {
        let minEdgeX = screenSize.width / CGFloat(marginPercents * 2)
        let maxEdgeX = screenSize.width - screenSize.width / CGFloat(marginPercents * 2)
        
        let minEdgeY = screenSize.height / CGFloat(marginPercents * 2)
        let maxEdgeY = screenSize.height - screenSize.height / CGFloat(marginPercents * 2)
        
        let stepX = CGFloat(step) * (screenSize.width - screenSize.width / CGFloat(marginPercents)) / deltaXValues
        let stepY = CGFloat(step) * (screenSize.height - screenSize.height / CGFloat(marginPercents)) / deltaYValues
        
        var startPointX = screenSize.width / 2 - stepX
        
        while (startPointX > minEdgeX) {
            self.move(to: CGPoint(x: startPointX, y: minEdgeY))
            self.addLine(to: CGPoint(x: startPointX, y: maxEdgeY))
            startPointX -= stepX
        }
        
        startPointX = screenSize.width / 2 + stepX
        
        while (startPointX < maxEdgeX) {
            self.move(to: CGPoint(x: startPointX, y: minEdgeY))
            self.addLine(to: CGPoint(x: startPointX, y: maxEdgeY))
            startPointX += stepX
        }
        
        var startPointY = screenSize.height / 2 - stepY
        
        while (startPointY > minEdgeY) {
            self.move(to: CGPoint(x: minEdgeX, y: startPointY))
            self.addLine(to: CGPoint(x: maxEdgeX, y: startPointY))
            startPointY -= stepY
        }
        
        startPointY = screenSize.height / 2 + stepY
        
        while (startPointY < maxEdgeY) {
            self.move(to: CGPoint(x: minEdgeX, y: startPointY))
            self.addLine(to: CGPoint(x: maxEdgeX, y: startPointY))
            startPointY += stepY
        }
        
    }
    
    mutating func drawChartWithBrokenLine(
        from data: [PointChart],
        screenSize: CGSize,
        deltaXValues: CGFloat,
        deltaYValues: CGFloat,
        marginPercents: Int
    ) {
        
        guard let firstPoint = data.first else {
            return
        }
        
        let halfW = screenSize.width / 2
        let halfH = screenSize.height / 2
        
        let sizeChartH = screenSize.height - screenSize.height / CGFloat(marginPercents)
        let sizeChartW = screenSize.width - screenSize.width / CGFloat(marginPercents)
        
        let firstCGPoint: CGPoint = CGPoint(
            x: CGFloat(firstPoint.x / deltaXValues) * sizeChartW + halfW,
            y: halfH - CGFloat(firstPoint.y / deltaYValues) * sizeChartH
        )
        
        self.move(to: firstCGPoint)
        
        for i in 1..<data.count {
            let point = CGPoint(
                x: data[i].x / deltaXValues * sizeChartW + halfW,
                y: halfH - data[i].y / deltaYValues * sizeChartH
            )
            self.addLine(to: point)
        }
    }
    
    mutating func drawChartWithSmoothedLine(
        from data: [PointChart],
        screenSize: CGSize,
        deltaXValues: CGFloat,
        deltaYValues: CGFloat,
        marginPercents: Int
    ) {
        
        guard let firstPoint = data.first else {
            return
        }
        
        let halfW = screenSize.width / 2
        let halfH = screenSize.height / 2
        
        let sizeChartH = screenSize.height - screenSize.height / CGFloat(marginPercents)
        let sizeChartW = screenSize.width - screenSize.width / CGFloat(marginPercents)
        
        var prevousPoint: CGPoint = CGPoint(
            x: (firstPoint.x / deltaXValues) * sizeChartW + halfW,
            y: halfH - (firstPoint.y / deltaYValues) * sizeChartH
        )
        
        self.move(to: prevousPoint)
               
        if (data.count == 2) {
            let point = CGPoint(
                x: (data[1].x / deltaXValues) * sizeChartW + halfW,
                y: halfH - (data[1].y / deltaYValues) * sizeChartH
            )
            self.addLine(to: point)
            return
        }

        var oldControlPoint: CGPoint?

        for i in 1..<data.count {
            let currentPoint = CGPoint(
                x: (data[i].x / deltaXValues) * sizeChartW + halfW,
                y: halfH - (data[i].y / deltaYValues) * sizeChartH
            )
            var nextPoint: CGPoint?
            if i < data.count - 1 {
                nextPoint = CGPoint(
                    x: (data[i + 1].x / deltaXValues) * sizeChartW + halfW,
                    y: halfH - (data[i + 1].y / deltaYValues) * sizeChartH
                )
            }

            let newControlPoint = controlPointForPoints(p1: prevousPoint, p2: currentPoint, next: nextPoint)

            self.addCurve(to: currentPoint, control1: oldControlPoint ?? prevousPoint, control2: newControlPoint ?? currentPoint)

            prevousPoint = currentPoint
            oldControlPoint = antipodalFor(point: newControlPoint, center: currentPoint)
        }
    }
    
    func antipodalFor(point: CGPoint?, center: CGPoint?) -> CGPoint? {
        guard let p1 = point, let center = center else {
            return nil
        }
        let newX = 2.0 * center.x - p1.x
        let newY = 2.0 * center.y - p1.y
        
        return CGPoint(x: newX, y: newY)
    }
    
    func midPointForPoints(p1: CGPoint, p2: CGPoint) -> CGPoint {
        return CGPoint(x: (p1.x + p2.x) / 2, y: (p1.y + p2.y) / 2);
    }
    
    func controlPointForPoints(p1: CGPoint, p2: CGPoint, next p3: CGPoint?) -> CGPoint? {
        guard let p3 = p3 else {
            return nil
        }
        
        let leftMidPoint  = midPointForPoints(p1: p1, p2: p2)
        let rightMidPoint = midPointForPoints(p1: p2, p2: p3)
        
        var controlPoint = midPointForPoints(p1: leftMidPoint, p2: antipodalFor(point: rightMidPoint, center: p2)!)
        
        if p1.y.between(a: p2.y, b: controlPoint.y) {
            controlPoint.y = p1.y
        } else if p2.y.between(a: p1.y, b: controlPoint.y) {
            controlPoint.y = p2.y
        }
        
        let imaginContol = antipodalFor(point: controlPoint, center: p2)!
        if p2.y.between(a: p3.y, b: imaginContol.y) {
            controlPoint.y = p2.y
        }
        if p3.y.between(a: p2.y, b: imaginContol.y) {
            let diffY = abs(p2.y - p3.y)
            controlPoint.y = p2.y + diffY * (p3.y < p2.y ? 1 : -1)
        }
        
        controlPoint.x += (p2.x - p1.x) * 0.1
        
        return controlPoint
    }
}

extension CGFloat {
    func between(a: CGFloat, b: CGFloat) -> Bool {
        return self >= Swift.min(a, b) && self <= Swift.max(a, b)
    }
}
