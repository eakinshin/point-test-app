//
//  PointsChartDate.swift
//  PointsTestApp
//
//  Created by EAkinshin on 13.07.2021.
//

import CoreGraphics
import Foundation

class PointsChartData {
    
    var pointsData: [PointChart] = []
    
    var minX: CGFloat = CGFloat.greatestFiniteMagnitude
    var maxX: CGFloat = CGFloat.leastNormalMagnitude
    
    var minY: CGFloat = CGFloat.greatestFiniteMagnitude
    var maxY: CGFloat = CGFloat.leastNormalMagnitude
    
    var deltaX: CGFloat = 0
    var deltaY: CGFloat = 0
    
    init(points: [Point]) {
        guard points.count > 0 else {
            return
        }
        self.pointsData = points
            .sorted{ $0.x < $1.x }
            .map{
                let x = CGFloat(truncating: $0.x as NSDecimalNumber)
                let y = CGFloat(truncating: $0.y as NSDecimalNumber)
                if x > maxX { maxX = x }
                if x < minX { minX = x }
                if y > maxY { maxY = y }
                if y < minY { minY = y }
                return PointChart(x: x, y: y)
            }
        deltaX = abs(abs(maxX) > abs(minX) ? maxX * 2 : minX * 2)
        deltaY = abs(abs(maxY) > abs(minY) ? maxY * 2 : minY * 2)
    }
}

struct PointChart : Hashable, Equatable {
    let x: CGFloat
    let y: CGFloat
}
