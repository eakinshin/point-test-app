//
//  PointsTableView.swift
//  PointsTestApp
//
//  Created by EAkinshin on 12.07.2021.
//

import SwiftUI

struct PointsTableView: View {
    
    var pointsChartData: PointsChartData
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            
            HStack{
                Spacer()
                Text("table_view_x_column".localized())
                Spacer()
                Text("table_view_y_column".localized())
                Spacer()
            }
            
            Divider()
            
            ForEach(self.pointsChartData.pointsData, id: \.self) { point in
                HStack{
                    Spacer()
                    Text(String(format: "%.2f", point.x))
                    Spacer()
                    Text(String(format: "%.2f", point.y))
                    Spacer()
                }
            }
        }
        .padding()
    }
}
