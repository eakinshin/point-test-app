//
//  PointsTabBarView.swift
//  PointsTestApp
//
//  Created by EAkinshin on 12.07.2021.
//

import SwiftUI

struct PointsTabBarView: View {
    
    var pointsChartData: PointsChartData
    
    @State private var selectedTab = 0
        
    var body: some View {
        TabView(selection: self.$selectedTab) {
            PointsTableView(pointsChartData: self.pointsChartData)
            .tabItem {
                Image(systemName: "text.justify")
                    .renderingMode(.template)
                    .foregroundColor(.blue)
            }
            .tag(0)

            PointsChartView(pointsChartData: self.pointsChartData)
            .tabItem {
                Image(systemName: "waveform.path.ecg")
                    .renderingMode(.template)
                    .foregroundColor(.blue)
            }
            .tag(1)
        }
        .accentColor(.blue)
        .navigationTitle(selectedTab == 0 ? "table_view_title".localized() : "chart_view_title".localized())
        .navigationBarTitleDisplayMode(.inline)
    }
}
