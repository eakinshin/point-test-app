//
//  PointsChartView.swift
//  PointsTestApp
//
//  Created by EAkinshin on 12.07.2021.
//

import SwiftUI

struct PointsChartView: View {
    
    var pointsChartData: PointsChartData
       
    var body: some View {
        ChartView(pointsChartData: self.pointsChartData)
    }
}
