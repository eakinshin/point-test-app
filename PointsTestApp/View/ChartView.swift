//
//  ChartView.swift
//  PointsTestApp
//
//  Created by EAkinshin on 10.07.2021.
//

import SwiftUI

struct ChartView: View {
    
    @State private var isBrokenLine: Bool = true
    
    var pointsChartData: PointsChartData
    
    let marginPercents = 10
    let chartNetStep = 10
    
    var body: some View {
        
        ZStack {
            VStack {
                
                HStack {
                    Text(String(format: "chart_view_net_step".localized(), self.chartNetStep))
                    
                    Spacer()
                    
                    Button(action: {
                        DispatchQueue.main.async {
                            self.isBrokenLine = true
                        }
                    }) {
                        Text("chart_view_broken_button_text".localized())
                    }
                    .disabled(self.isBrokenLine ? true : false)
                    
                    Button(action: {
                        DispatchQueue.main.async {
                            self.isBrokenLine = false
                        }
                    }) {
                        Text("chart_view_smoothed_button_text".localized())
                    }
                    .disabled(self.isBrokenLine ? false : true)
                    
                }
                .padding(EdgeInsets(top: 16, leading: 16, bottom: 0, trailing: 16))
                
                GeometryReader { reader in
                    if pointsChartData.pointsData.count > 0 {
                        Path { p in
                            if self.isBrokenLine {
                                p.drawChartWithBrokenLine(
                                    from: pointsChartData.pointsData,
                                    screenSize: reader.size,
                                    deltaXValues: pointsChartData.deltaX,
                                    deltaYValues: pointsChartData.deltaY,
                                    marginPercents: self.marginPercents
                                )
                            } else {
                                p.drawChartWithSmoothedLine(
                                    from: pointsChartData.pointsData,
                                    screenSize: reader.size,
                                    deltaXValues: pointsChartData.deltaX,
                                    deltaYValues: pointsChartData.deltaY,
                                    marginPercents: self.marginPercents
                                )
                            }
                        }
                        .stroke(Color.red, style: StrokeStyle(lineWidth: 1))
                        
                        Path { p in
                            p.drawAxis(screenSize: reader.size, marginPercents: self.marginPercents)
                        }
                        .stroke(Color.black, style: StrokeStyle(lineWidth: 2))
                        
                        Path { p in
                            p.drawNet(
                                screenSize: reader.size,
                                deltaXValues: pointsChartData.deltaX,
                                deltaYValues: pointsChartData.deltaY,
                                marginPercents: self.marginPercents,
                                step: self.chartNetStep
                            )
                        }
                        .stroke(Color.gray, style: StrokeStyle(lineWidth: 1))
                    }
                }
            }
        }
    }
}
