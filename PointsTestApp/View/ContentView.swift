//
//  ContentView.swift
//  PointsTestApp
//
//  Created by EAkinshin on 07.07.2021.
//

import SwiftUI

struct ContentView: View {
    
    @State private var resultPoints: [CGPoint] = []
    
    var body: some View {
        MainView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
