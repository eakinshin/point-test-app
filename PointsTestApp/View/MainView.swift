//
//  MainView.swift
//  PointsTestApp
//
//  Created by EAkinshin on 12.07.2021.
//

import SwiftUI

struct MainView: View {
    
    @ObservedObject var mainVM = MainViewModel()
    
    var body: some View {
        NavigationView{
            ZStack {
                
                NavigationLink(
                    destination: PointsTabBarView(pointsChartData: self.mainVM.pointsChartData),
                    isActive: self.$mainVM.goToPointsTabBar
                ) { EmptyView() }
                
                VStack {
                    HStack {
                        Text("main_view_points_count_label".localized())
                        Spacer()
                    }
                    TextField("main_view_points_count_placeholder".localized(), text: self.$mainVM.pointsCountString)
                        .keyboardType(.numberPad)
                        .foregroundColor(self.mainVM.isPointsCountValid() ? .black : .red)
                        
                    HStack {
                        Text(self.mainVM.errorOnValidatePointsCount())
                            .font(.system(size: 12))
                            .foregroundColor(.red)
                        Spacer()
                    }
                    
                    Button(action: {
                        self.mainVM.fetchPoints()
                    }) {
                        Text("main_view_go_button_text".localized())
                    }
                    .disabled(self.mainVM.isGoButtonDisabled() || self.mainVM.isLoading)
                }
                .padding()
                
                if self.mainVM.fetchDataError != nil {
                    VStack {
                        Text(self.mainVM.fetchDataError!)
                            .foregroundColor(Color.white)
                            .multilineTextAlignment(.leading)
                            .padding()
                    }
                    .background(Color.red)
                    .cornerRadius(10)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color.red, lineWidth: 2)
                    )
                    .padding()
                }
                
                if self.mainVM.isLoading {
                    VStack(alignment: .center) {
                        ProgressView()
                            .progressViewStyle(CircularProgressViewStyle())
                    }
                }
            }
        }
    }
}
