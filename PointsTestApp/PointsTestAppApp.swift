//
//  PointsTestAppApp.swift
//  PointsTestApp
//
//  Created by EAkinshin on 07.07.2021.
//

import SwiftUI

@main
struct PointsTestAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
